
package graph;

import Controller.Controller;
import Model.Model;
import View.View;

public class Graph {
    
   private static Model model = null;
   private static Controller controller = null;
   private static View view = null;    
   
    public static void main(String[] args) {
    
    controller = new Controller (model, view);    
    model = new Model( view);
    view = new View(model);
    }
    
}
