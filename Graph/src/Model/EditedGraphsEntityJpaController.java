/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.exceptions.NonexistentEntityException;
import Model.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author karty
 */
public class EditedGraphsEntityJpaController implements Serializable {

    public EditedGraphsEntityJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EditedGraphsEntity editedGraphsEntity) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(editedGraphsEntity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEditedGraphsEntity(editedGraphsEntity.getId()) != null) {
                throw new PreexistingEntityException("EditedGraphsEntity " + editedGraphsEntity + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EditedGraphsEntity editedGraphsEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            editedGraphsEntity = em.merge(editedGraphsEntity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigInteger id = editedGraphsEntity.getId();
                if (findEditedGraphsEntity(id) == null) {
                    throw new NonexistentEntityException("The editedGraphsEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigInteger id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EditedGraphsEntity editedGraphsEntity;
            try {
                editedGraphsEntity = em.getReference(EditedGraphsEntity.class, id);
                editedGraphsEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The editedGraphsEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(editedGraphsEntity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EditedGraphsEntity> findEditedGraphsEntityEntities() {
        return findEditedGraphsEntityEntities(true, -1, -1);
    }

    public List<EditedGraphsEntity> findEditedGraphsEntityEntities(int maxResults, int firstResult) {
        return findEditedGraphsEntityEntities(false, maxResults, firstResult);
    }

    private List<EditedGraphsEntity> findEditedGraphsEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EditedGraphsEntity.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EditedGraphsEntity findEditedGraphsEntity(BigInteger id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EditedGraphsEntity.class, id);
        } finally {
            em.close();
        }
    }

    public int getEditedGraphsEntityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EditedGraphsEntity> rt = cq.from(EditedGraphsEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
