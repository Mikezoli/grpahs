
package Model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EditedGraphs implements Serializable {

    private static final long serialVersionUID = 1L;
   @Id
    @Column(name="id")
    private BigInteger id;
    @Column(name="point_start")
    private BigInteger point_start;
    @Column(name="point_end")
    private BigInteger point_end;
    
     public EditedGraphs(BigInteger id, BigInteger point_start, BigInteger point_end) {
        this.id=id;
        this.point_start = point_start;
        this.point_end = point_end;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }
    
    public BigInteger getPoint_start() {
        return point_start;
    }

    public void setPoint_start(BigInteger point_start) {
        this.point_start = point_start;
    }

    public BigInteger getPoint_end() {
        return point_end;
    }

    public void setPoint_end(BigInteger point_end) {
        this.point_end = point_end;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EditedGraphs)) {
            return false;
        }
        EditedGraphs other = (EditedGraphs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.EditedGraphs[ id=" + id + " ]";
    }
    
}
