package Model;

import Controller.Controller;
import View.View;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import org.eclipse.persistence.jpa.JpaHelper;


    class Graph {
        BigInteger id;
        BigInteger start_point;
        BigInteger end_point;

        public Graph(BigInteger id, BigInteger start_point, BigInteger end_point) {
            this.id = id;
            this.start_point = start_point;
            this.end_point = end_point;
        }

    Graph() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        public BigInteger getId() {
            return id;
        }

        public void setId(BigInteger id) {
            this.id = id;
        }

        public BigInteger getStart_point() {
            return start_point;
        }

        public void setStart_point(BigInteger start_point) {
            this.start_point = start_point;
        }

        public BigInteger getEnd_point() {
            return end_point;
        }

        public void setEnd_point(BigInteger end_point) {
            this.end_point = end_point;
        }

        @Override
        public String toString() {
            return "Graph{" + "id=" + id + ", start_point=" + start_point + ", end_point=" + end_point + '}';
        }
        
        
        
    }
public class Model {
    
    private Controller controller;
    private ArrayList<GraphDTO> graphsList = new ArrayList<>();
    private EntityManagerFactory emf = null;
    

    public Model(View view) {
      emf=Persistence.createEntityManagerFactory("GraphPU");
        
    }
    
   public void setController(){
       this.controller=controller;
       
   }

    public ArrayList<GraphDTO> getGraphsList() {
        return graphsList;
    }

    public void setGraphsList(ArrayList<GraphDTO> graphsList) {
        this.graphsList = graphsList;
    }
   
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    public void uploadDB(){
        EntityManager em = null;
        for (int i = 0; i <graphsList.size()-1; i++) {          
            
            Graph graph = new Graph();

            em = getEntityManager();
            em.getTransaction().begin();
            graph.setId(graphsList.get(i).getId());
            graph.setStart_point(graphsList.get(i).getStart_point());
            graph.setEnd_point(graphsList.get(i).getEnd_point());
            em.persist(graph);
            em.close();     
        }
    }

}
