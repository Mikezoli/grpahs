
package Model;


import java.io.Serializable;
import java.math.BigInteger;

public class GraphDTO implements Serializable{
    
private BigInteger id;
private BigInteger start_point;
private BigInteger end_point;

    public GraphDTO(BigInteger id, BigInteger start_point, BigInteger end_point) {
        this.id = id;
        this.start_point = start_point;
        this.end_point = end_point;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getStart_point() {
        return start_point;
    }

    public void setStart_point(BigInteger start_point) {
        this.start_point = start_point;
    }

    public BigInteger getEnd_point() {
        return end_point;
    }

    public void setEnd_point(BigInteger end_point) {
        this.end_point = end_point;
    }


            
}
