/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.exceptions.NonexistentEntityException;
import Model.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author karty
 */
public class OriginalGraphsEntityJpaController implements Serializable {

    public OriginalGraphsEntityJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OriginalGraphsEntity originalGraphsEntity) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(originalGraphsEntity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOriginalGraphsEntity(originalGraphsEntity.getId()) != null) {
                throw new PreexistingEntityException("OriginalGraphsEntity " + originalGraphsEntity + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OriginalGraphsEntity originalGraphsEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            originalGraphsEntity = em.merge(originalGraphsEntity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                BigInteger id = originalGraphsEntity.getId();
                if (findOriginalGraphsEntity(id) == null) {
                    throw new NonexistentEntityException("The originalGraphsEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(BigInteger id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OriginalGraphsEntity originalGraphsEntity;
            try {
                originalGraphsEntity = em.getReference(OriginalGraphsEntity.class, id);
                originalGraphsEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The originalGraphsEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(originalGraphsEntity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OriginalGraphsEntity> findOriginalGraphsEntityEntities() {
        return findOriginalGraphsEntityEntities(true, -1, -1);
    }

    public List<OriginalGraphsEntity> findOriginalGraphsEntityEntities(int maxResults, int firstResult) {
        return findOriginalGraphsEntityEntities(false, maxResults, firstResult);
    }

    private List<OriginalGraphsEntity> findOriginalGraphsEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OriginalGraphsEntity.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OriginalGraphsEntity findOriginalGraphsEntity(BigInteger id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OriginalGraphsEntity.class, id);
        } finally {
            em.close();
        }
    }

    public int getOriginalGraphsEntityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OriginalGraphsEntity> rt = cq.from(OriginalGraphsEntity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
